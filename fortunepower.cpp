#include <QDebug>
#include "fortunepower.h"
#include "game.h"
#include "deck.h"
#include "card.h"
#include "string.h"
#include "colours.h"
#include "fortunewindow.h"
#include <QThread>

FortunePower::FortunePower(FortuneWindow* fw)
{
    this->fortuneWindow = fw;
}

void FortunePower::usePower(Color guessCol, int guessNum, Game &game) {
    Card* card = game.getDeck()->getOneCard();
    QString cardStr = card->toString();
    qDebug() << "Card to quess: " << cardStr;
    if (guessCol == card->getColor() || guessNum == card->getValue()) {
        game.awardPoint();
        fortuneWindow->showWin();
    }
    else {
        fortuneWindow->showLose();
    }
    QThread::sleep(5);
    fortuneWindow->hide();
    game.backToGameWindow();
}
