#ifndef COLOURS_H
#define COLOURS_H
#include <iostream>

enum class Color {BLUE, RED, GREEN, YELLOW, VIOLET, PINK, ORANGE, NONE};

#endif // COLOURS_H
