#ifndef ColorPower_H
#define ColorPower_H

#include "colours.h"
#include "power.h"

class Game;

class ColorPower : public Power
{
private:
    Color color;
public:
    ColorPower();
    void usePower(Game& game, Color color);
};

#endif
