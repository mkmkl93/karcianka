#include <algorithm>
#include <ctime>
#include <QDebug>
#include "deck.h"
#include "game.h"
#include "card.h"
#include "sinpower.h"
#include "colorpower.h"
#include "virtuespower.h"
#include "fortunepower.h"


Deck::Deck(Game* game): game(game) {
    //BLUE, RED, GREEN, YELLOW, VIOLET, PINK, ORANGE, NONE
    std::srand ( unsigned ( std::time(0) ) );
    std::vector<Color> colors;
    colors.push_back(Color::RED);
    colors.push_back(Color::ORANGE);
    colors.push_back(Color::YELLOW);
    colors.push_back(Color::GREEN);
    colors.push_back(Color::BLUE);
    colors.push_back(Color::VIOLET);
    colors.push_back(Color::PINK);

    std::vector<CardType> types;
    types.push_back(CardType::SEAS);
    types.push_back(CardType::FORTUNES);
    types.push_back(CardType::WONDERS);
    types.push_back(CardType::VIRTUES);
    types.push_back(CardType::STAGES_OF_LIFE);
    types.push_back(CardType::COLORS);
    types.push_back(CardType::SINS);

    for (int i = 0; i < 7; ++i) {
        for (int j = 0; j < 7; ++j) {
            Card * card;
            switch (i) {
                case 0: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new Power); break;}
                case 1: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new Power); break;}
                case 2: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new Power); break;}
                case 3: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new VirtuesPower); break;}
                case 4: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new Power); break;}
                case 5: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new ColorPower); break;}
                case 6: {card = new Card((j + i) % 7 + 1, colors[j], types[i], game, new SinPower); break;}
            }
            cards.push_back(card);
        }
    }
    std::random_shuffle(cards.begin(), cards.end());
}

void Deck::addCard(Card* card) {
    this->cards.push_back(card);
}

void Deck::shuffleCards() {
    std::random_shuffle(cards.begin(), cards.end());
}

Card* Deck::getOneCard() {
    Card* result = cards.back();
    cards.pop_back();
    return result;
}

int Deck::size() {
    return cards.size();
}

std::vector<Card*> Deck::getThreeCards() {
    std::vector<Card*> result;
    for (int i = 0; i < 3; i++) {
        Card* card = cards.back();
        result.push_back(card);
        cards.pop_back();
    }
    return result;
}

void Deck::add(Card *card) {
    this->addCard(card);
}
