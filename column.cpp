#include "column.h"
#include "card.h"

Column::Column()
{
cards.resize(0);
}

int Column::getSize(){
    return cards.size();
}

void Column::addCard(Card* card){
    this->cards.push_back(card);
}

Card* Column::getLastCard(){
    Card* result = this->cards.back();
    cards.pop_back();
    return result;
}
