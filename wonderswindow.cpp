#include "wonderswindow.h"
#include "ui_wonderswindow.h"
#include "game.h"
#include <QDebug>
#include "player.h"

WondersWindow::WondersWindow(QWidget *parent, Game* game) :
    QDialog(parent),
    ui(new Ui::WondersWindow),
    game(game)
{
    ui->setupUi(this);
    connect(ui->OK, &QAbstractButton::clicked,
            this, [this]{this->returnToGameWindow();});
    connect(ui->deck, &QAbstractButton::clicked,
            this, [this]{this->showCardsFromDeck();});
    connect(ui->opponent, &QAbstractButton::clicked,
            this, [this]{this->showOpponentsCards();});
}

WondersWindow::~WondersWindow()
{
    delete ui;
}

void WondersWindow::setCardsLabels(std::vector<Card *> cards) {
    QPixmap pixmap1(cards[0]->getFilePath());
    pixmap1 = pixmap1.scaled(120, 120);
    ui->card1->setPixmap(pixmap1);

    QPixmap pixmap2(cards[1]->getFilePath());
    pixmap2 = pixmap2.scaled(120, 120);
    ui->card2->setPixmap(pixmap2);

    QPixmap pixmap3(cards[2]->getFilePath());
    pixmap3 = pixmap3.scaled(120, 120);
    ui->card3->setPixmap(pixmap3);
}

void WondersWindow::showOpponentsCards()
{
    ui->opponent->setEnabled(false);
    ui->deck->setEnabled(false);
    std::vector<Player*> players = game->getPlayers();
    int whoseTurn = game->getWhoseTurn();
    std::vector<Card*> cards =  players[whoseTurn]->getCards();
    setCardsLabels(cards);

    qDebug () << "fortune window ok clicked";
}

void WondersWindow::showCardsFromDeck()
{
    ui->opponent->setEnabled(false);
    ui->deck->setEnabled(false);
    Deck *deck = game->getDeck();
    std::vector<Card*> cards = deck->getThreeCards();
    setCardsLabels(cards);
    for (int i = 0; i < 3; i++) {
        deck->add(cards[i]);
    }
    qDebug () << "fortune window ok clicked";
}

void WondersWindow::returnToGameWindow()
{
    this->hide();
    game->backToGameWindow();
}

