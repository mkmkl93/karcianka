#ifndef CARD_H
#define CARD_H

#include <QString>
#include "cardtype.h"
#include "colours.h"

class Power;
class Game;
class Player;
class Column;
//! \brief Each card has its own value, color and type. Each cardType has corresponding power.
class Card {
private:
    int value; ///< Value of card, used to count total score.
    Color color;
    CardType cardType;
    Power *power; ///< Current power can be different than power corresponding to card type.
    Game *game; ///< Pointer to game so that card can interact with it.

public:
    Card();
    Card(int value, Color color, CardType cardType, Game* game, Power* power):
        value(value), color(color), cardType(cardType), power(power), game(game) {}

    //! \brief Card was chosen by button click, we need to put it on the board.
   // void chosen();
    //! \brief Use power of card (after being put on board).
    void usePower();
    void usePower(Player* player);
    void usePower(Column* column);
    void showReverse();
    void showReverseAsAvailable();
    void showObverseAsAvailable();
    CardType getCardType();
    int getValue();
    //! \brief Get string that represents card so that we can print it or display on board.
    QString toString();
    QString getFilePath();
    Color getColor();
    void setPower(Power *power);
};

#endif // CARD_H
