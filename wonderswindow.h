#ifndef WONDERWINDOW_H
#define WONDERSWINDOW_H

#include <QDialog>
#include "colours.h"
#include "card.h"

class Game;

namespace Ui {
class WondersWindow;
}

class WondersWindow : public QDialog
{
    Q_OBJECT
public:
    explicit WondersWindow(QWidget *parent = nullptr, Game* game = nullptr);
    ~WondersWindow();
    void showCardsFromDeck();
    void showOpponentsCards();
    void returnToGameWindow();
    void setCardsLabels(std::vector<Card*>);

private:
    Ui::WondersWindow *ui;
    Game* game;
};

#endif // WONDERSWINDOW_H
