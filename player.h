#ifndef PLAYER_H
#define PLAYER_H
#include <vector>
#include "card.h"
#include "deck.h"

class Game;

class Player {
private:
    std::vector<Card*> cards;
    Game* game;
    void chooseColorOrNumber();
    int guessedCards;
    QString name;

public:
    Player(QString &name, Game* game);
    void printHand();
    void drawCard();
    Card * getCard(int number);
    //! \brief Add card to player's hand.
    void addCard (Card* card);
    //! \brief Remove chosen card from player's hand.
    //! change pointer to NULL
    void removeCard(int number);
    void play();
    void incGuessedCards();
    //void playCard(int number);
    std::vector<Card*> getCards();
    int getGuessedCards();
    QString getName();
};
#endif // PLAYER_H
