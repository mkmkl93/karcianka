#ifndef COLUMN_H
#define COLUMN_H
#include <vector>

class Card;

//! \brief Column on a board. Each column stores cards of the same type.
//!
class Column {
private:
    std::vector<Card*> cards; ///< Maximum 7 cards in a column.
public:
    Column();
    bool isCardCorresponding(Card* card);
    //! \brief Get number of cards put in column.
    int getSize();
    void showAsAvailable();
    void addCard(Card* card);
    //! \brief Get last card that was put in column.
    Card *getLastCard();
};

#endif // COLUMN_H
