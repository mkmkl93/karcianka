#ifndef CLICKABLEFRAME_H
#define CLICKABLEFRAME_H

#include <QObject>
#include <QWidget>
#include <QFrame>
#include <QMouseEvent>
#include <QMouseEventTransition>

class ClickableFrame : public QFrame
{
    Q_OBJECT

public:
    explicit ClickableFrame(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
        ~ClickableFrame();
    void setActive(bool active);
    bool getActive();

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent* event);

private:
    bool active;
};

#endif // CLICKABLEFRAME_H
