#ifndef POWER_H
#define POWER_H

class Player;
class Column;
class Game;

class Power {
public:
    Power();
    virtual void printPower();
    virtual void usePower(Game* game);
    virtual void usePower(Player* player, Game* game);
    virtual void usePower(Column* column, Game* game);
};

#endif // POWER_H
