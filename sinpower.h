#ifndef SINPOWER_H
#define SINPOWER_H
#include "power.h"

class SinPower : public Power
{
public:
    SinPower();
    void printPower();
    void usePower(Game* game);
    void usePower(Column *column, Game* game);
    void usePower(Player* player, Game* game);
};

#endif // SINPOWER_H
