#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class GameWindow;

/*!
 * \brief Manages current active window
 *
 * \sa GameWindow
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void BackToMenu();

private slots:
    void startNewGame();
    void showHistory();


private:
    Ui::MainWindow* ui;
    GameWindow* gameWindow;
    QLabel * names[10];
    QLabel * scores[10];
};

#endif // MAINWINDOW_H
