#ifndef DECK_H
#define DECK_H
#include <vector>
#include <algorithm>

class Card;
class Game;
//! \brief Deck stores all cards that players can take.
class Deck {
private:
    Game* game;
    std::vector<Card*> cards; ///< Cards in deck.
    void addCard(Card* card);

public:
    Deck(Game* game);
    void shuffleCards();
    int size();
    Card* getOneCard();
    std::vector<Card*> getThreeCards();
    void add(Card* card);
};
#endif // DECK_H
