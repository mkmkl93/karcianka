#include "fortunewindow.h"
#include "ui_fortunewindow.h"
#include "fortunepower.h"
#include <QDebug>

FortuneWindow::FortuneWindow(QWidget *parent, Game* game) :
    QDialog(parent),
    ui(new Ui::FortuneWindow),
    game(game)
{
    ui->setupUi(this);
    connect(ui->blue, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::BLUE);});
    connect(ui->red, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::RED);});
    connect(ui->orange, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::ORANGE);});
    connect(ui->violet, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::VIOLET);});
    connect(ui->green, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::GREEN);});
    connect(ui->pink, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::PINK);});
    connect(ui->yellow, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::YELLOW);});
    connect(ui->yellow, &QAbstractButton::clicked,
            this, [this]{this->guessColour(Color::YELLOW);});
    connect(ui->one, &QAbstractButton::clicked,
            this, [this]{this->guessNum(1);});
    connect(ui->two, &QAbstractButton::clicked,
            this, [this]{this->guessNum(2);});
    connect(ui->three, &QAbstractButton::clicked,
            this, [this]{this->guessNum(3);});
    connect(ui->four, &QAbstractButton::clicked,
            this, [this]{this->guessNum(4);});
    connect(ui->five, &QAbstractButton::clicked,
            this, [this]{this->guessNum(5);});
    connect(ui->six, &QAbstractButton::clicked,
            this, [this]{this->guessNum(6);});
    connect(ui->seven, &QAbstractButton::clicked,
            this, [this]{this->guessNum(7);});
}

FortuneWindow::~FortuneWindow()
{
    delete ui;
}

void FortuneWindow::showWin()
{
    ui->res->setText("You have chosen well, my friend.");
    ui->res->repaint();
}

void FortuneWindow::showLose()
{
    qDebug() << "showLose()";
    ui->res->setText("We're not very fortunate today, are we?");
    ui->res->repaint();
}

void FortuneWindow::guessColour(Color c)
{
    qDebug () << "guessing colour";
    FortunePower* power = new FortunePower(this);
    qDebug() << "created new fortune power";
    power->usePower(c, -1, *game);
    qDebug() << "used fortune power";
}

void FortuneWindow::guessNum(int num)
{
    FortunePower* power = new FortunePower(this);
    power->usePower(Color::NONE, num, *game);
}
