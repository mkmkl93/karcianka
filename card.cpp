#include "card.h"
#include "player.h"
#include "power.h"
#include "column.h"
#include "sinpower.h"
#include "fortunepower.h"
#include "colorpower.h"
#include "game.h"
#include <QDebug>

Card::Card() {}

CardType Card::getCardType(){
    return cardType;
}

int Card::getValue() {
    return this->value;
}

QString Card::toString() {
    QString result = QString::number(this->value);
    switch (this->cardType) {
        case CardType::NO_TYPE : result.append("\nNO_TYPE"); break;
        case CardType::SINS : result.append("\nSIN"); break;
        case CardType::VIRTUES : result.append("\nVIRTUE"); break;
        case CardType::STAGES_OF_LIFE : result.append("\nSTAGE"); break;
        case CardType::SEAS : result.append("\nSEA"); break;
        case CardType::COLORS : result.append("\nCOLOR"); break;
        case CardType::FORTUNES : result.append("\nFORTUNE"); break;
        case CardType::WONDERS : result.append("\nWONDERS"); break;
    }
    result.append("\n");
    switch (this->color) {
        case Color::BLUE : result.append("\nBLUE"); break;
        case Color::RED : result.append("\nRED"); break;
        case Color::GREEN : result.append("\nGREEN"); break;
        case Color::YELLOW : result.append("\nYELLOW"); break;
        case Color::VIOLET : result.append("\nVIOLET"); break;
        case Color::PINK : result.append("\nPINK"); break;
        case Color::ORANGE : result.append("\nORANGE"); break;
        case Color::NONE : result.append(" NONE"); break;
    }
    return result;
}

Color Card::getColor() {
    return color;
}

void Card::usePower() {
    qDebug() << "USE POWER () " ;
    qDebug() << toString();
    power->printPower();
    power->usePower(game);
}

void Card::usePower(Player* player) {
   qDebug() << "USE POWER (Player* player) " ;
   qDebug() << toString();
   power->printPower();
   power->usePower(player, game);
}

void Card::usePower(Column* column) {
    return;
}

void Card::setPower(Power *power) {
    this->power = power;
}

QString Card::getFilePath(){
    QString result = ":/cards/";
    switch (this->cardType) {
        case CardType::NO_TYPE : return result;
        case CardType::SINS : result.append("sins_"); break;
        case CardType::VIRTUES : result.append("virtues_"); break;
        case CardType::STAGES_OF_LIFE : result.append("stages_"); break;
        case CardType::SEAS : result.append("seas_"); break;
        case CardType::COLORS : result.append("colors_"); break;
        case CardType::FORTUNES : result.append("fortunes_"); break;
        case CardType::WONDERS : result.append("wonders_"); break;
    }
    result.append(QString::number(this->value));
    result.append(".png");
    return result;
}
