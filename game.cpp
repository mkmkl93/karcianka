#include <QDebug>
#include <QPushButton>
#include <QString>
#include <QMessageBox>
#include <QList>
#include <QPair>
#include <assert.h>
#include <vector>

#include "game.h"
#include "player.h"
#include "column.h"
#include "gamewindow.h"
#include "cardtype.h"
#include "card.h"
#include "mainwindow.h"

std::map<CardType,int> Game::whichColumn = {
    {CardType::SINS,             0},
    {CardType::VIRTUES,          1},
    {CardType::STAGES_OF_LIFE,   2},
    {CardType::SEAS,             3},
    {CardType::COLORS,           4},
    {CardType::FORTUNES,         5},
    {CardType::WONDERS,          6}
};

//DEBUG
QString MFtoString( MovePhase m ){
    QString result;
    switch (m) {
        case MovePhase::START : result = "START"; break;
        case MovePhase::PUT_ON_BOARD : result = "PUT_ON_BORD"; break;
        case MovePhase::CARD_POWER_PHASE : result = "CARD_POWER_FAZE"; break;
        case MovePhase::SINS_PHASE : result = "SINS_FAZE"; break;
        //case MoveFaze::END_TOUR : result = "END_TOUR"; break;
    }
    return result;
}

Game::Game(std::vector<QString>* playersName):
    trump(Color::NONE),
    cardPlayedAs(CardType::NO_TYPE),
    playedCard(NULL),
    phase(MovePhase::PUT_ON_BOARD),
    selectedCard(-1),
    whoseTurn(0),
    numberOfPlayers(2)
{
    numberOfPlayers = playersName->size();
    deck = new Deck(this);
    for (int i = 0; i < numberOfPlayers; i++)
        players.push_back(new Player(playersName->at(i), this));
    for (int i = 0; i < 7; i++)
        board.push_back(new Column);
}

void Game::endGame()
{
    this->gameWindow->setCards(players[whoseTurn]->getCards());
    int winnerPts = 0, playerPts = 0;
    QString winnerName;
    for (Player* player : players){
        playerPts = 2 * player->getGuessedCards();
        for (Card* card : player->getCards())
            playerPts += pointsForCard(card);
        if (playerPts > winnerPts){
            winnerPts = playerPts;
            winnerName = player->getName();
        }
    }
    QString winnerMsg = tr("Player %1 wins with %2 points!").arg(winnerName, QString::number(winnerPts));
    updateBestScores(winnerName, QString::number(winnerPts));
    gameWindow->mainWindow->BackToMenu();
    emit gameOver(winnerMsg);
}

int Game::pointsForCard(Card* card) {
    int maxValue = 7 - board[whichColumn.at(CardType::STAGES_OF_LIFE)]->getSize();
    if (card->getColor() == trump)
        return maxValue;
    return card->getValue() > maxValue ? 0 : card->getValue();
}

void Game::nextTurn() {
    qDebug() << "NEW TOUR: LEFT IN DECK "<<deck->size();
    players[0] ->printHand();
    players[1] ->printHand();

    if(checkIfEndOfGame() || false){ //! koniec gry
        endGame();
        return;
    }

    whoseTurn = (whoseTurn + 1) % numberOfPlayers;
    qDebug() << "whoseTurn: " << whoseTurn;
    setPlayersNames();
    cardPlayedAs = CardType::NO_TYPE;
    gameWindow->setEnabledColumns({0, 0, 0, 0, 0, 0, 0});
    gameWindow->setCards(players[whoseTurn]->getCards());
}

void Game::setGameWindow(GameWindow* gameWindow) {
    this->gameWindow = gameWindow;
    this->gameWindow->setCards(players[whoseTurn]->getCards());
    phase = MovePhase::START;
    setPlayersNames();
    putCardOnBoard(deck->getOneCard());
}

void Game::takeCardFromBoard(int columnNum) {
    assert(board[columnNum]->getSize() > 0);
    gameWindow->takeCardFromBoard(columnNum, board[columnNum]->getSize()-1);
    Card* card = board[columnNum]->getLastCard();
    players[whoseTurn]->addCard(card);
    phase = MovePhase::START;
    nextTurn();
}

void Game::putCardOnBoard(Card* card) {

    CardType type = card->getCardType();
qDebug() << "PUT ON START" ;
    qDebug() << "column: " << whichColumn.at(type);
    qDebug() << "row: " << board[whichColumn.at(type)]->getSize();
    gameWindow->putCardOnBoard(card, whichColumn.at(type), board[whichColumn.at(type)]->getSize());
    board[whichColumn.at(type)]->addCard(card);
}

void Game::putCardOnBoardWithoutPower(int column) {
    Card* card = playedCard;
    qDebug() << "column: " << column;
    qDebug() << "row: " << board[column]->getSize();
    gameWindow->putCardOnBoard(card, column, board[column]->getSize());
    board[column]->addCard(card);
    if (card->getCardType() == CardType::COLORS) {
        changeTrump(card->getColor());
    }
    phase = MovePhase::START;
    qDebug()<<"NEXT TOUR";
    nextTurn();
}

void Game::putCardOnBoard(int column) {
    gameWindow->setButtonsColor();
    Card* card = players[whoseTurn]->getCard(selectedCard);
    players[whoseTurn]->removeCard(selectedCard);
    playedCard = card;

    if (phase != MovePhase::PUT_ON_BOARD ) {
        qDebug() << "WRONG MOVEFAZE  expected " << MFtoString(MovePhase::PUT_ON_BOARD) << " get "<< MFtoString(phase);
        return;
    }

    phase = MovePhase::CARD_POWER_PHASE;

    // If card has trump color then we change its power according to column.
    if (card->getColor() == trump) {
        if (whichColumn.at(CardType::SINS) == column) {
            card->setPower(new SinPower);
            cardPlayedAs = CardType::SINS;
        }
        if (whichColumn.at(CardType::SEAS) == column) {
            cardPlayedAs = CardType::SEAS;
        }
        if (whichColumn.at(CardType::COLORS) == column) {
            card->setPower(new ColorPower);
            cardPlayedAs = CardType::COLORS;
        }
        if (whichColumn.at(CardType::VIRTUES) == column) {
            card->setPower(new VirtuesPower);
            cardPlayedAs = CardType::VIRTUES;
        }
        if (whichColumn.at(CardType::WONDERS) == column) {
            cardPlayedAs = CardType::WONDERS;
        }
        if (whichColumn.at(CardType::FORTUNES) == column) {
            cardPlayedAs = CardType::FORTUNES;
        }
        if (whichColumn.at(CardType::STAGES_OF_LIFE) == column) {
            card->setPower(new StageOfLifePower);
            cardPlayedAs = CardType::STAGES_OF_LIFE;
        }
    }
    else {
        cardPlayedAs = card->getCardType();
    }

    if (cardPlayedAs != CardType::SEAS)
        players[whoseTurn]->drawCard();

    qDebug() << "column: " << column;
    qDebug() << "row: " << board[column]->getSize();
    gameWindow->putCardOnBoard(card, column, board[column]->getSize());
    board[column]->addCard(card);

    assert (card->getCardType() != CardType::NO_TYPE);

    if ( cardPlayedAs == CardType::SINS ) {
        phase = MovePhase::SINS_PHASE;
        qDebug() << "SIN PLAYED " << whoseTurn;
        playedCard->usePower(players[1 - whoseTurn]);
        setEnabledColumns(1 - whoseTurn, playedCard);
        return;
    }

    if ( cardPlayedAs == CardType::SEAS ) {
        setEnabledNotEmptyColumns();
        return;
    }

    if (cardPlayedAs == CardType::FORTUNES) {
        gameWindow->showFortunes();
    }

    if (cardPlayedAs == CardType::VIRTUES) {
        card->usePower();
    }

    if (cardPlayedAs == CardType::COLORS) {
        qDebug() << "changing trump color" << column;
        changeTrump(card->getColor());
    }

    if (cardPlayedAs == CardType::WONDERS) {
        gameWindow->showWonders();
    }

    phase = MovePhase::START;
    qDebug()<<"NEXT TURN";
    nextTurn();
}

/*! \brief card from players hand.
 */
void Game::selectCard(int num) {
    if (phase != MovePhase::START){
        qDebug() << "WRONG MOVEPHASE  expected " <<MFtoString(MovePhase::START) << " get "<< MFtoString(phase);
        return;
    }
    else {
        phase = MovePhase::PUT_ON_BOARD;
    }

    selectedCard = num;
    qDebug() <<"play Card: "<< selectedCard <<" "<< num;
    if (trump == Color::NONE) {
        qDebug() <<"trump none";
    }
    setEnabledColumns(whoseTurn, NULL);
}

/*! \brief active power of card in "card power faze" which required choosing player
 *
void Game::playerChosen() {
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = (buttonSender->objectName()).right(1); // retrive the text from the button clicked
    int number = (buttonText.toInt() + whoseTurn) % numberOfPlayers;
    qDebug() <<"player "<< number<< "was chosen  button: "<<buttonSender->objectName();
    //&& cardPlayedAs != CardType::WONDERS
    if ( phase != MovePhase::CARD_POWER_PHASE && (cardPlayedAs != CardType::SINS ))
    {
        qDebug() << "WRONG MOVEFAZE  expected " << MFtoString(MovePhase::CARD_POWER_PHASE) << " get "<< MFtoString(phase);
        return;
    }
    assert(playedCard != NULL);

}*/

void Game::changeTrump(Color color) {
    trump = color;
}

bool Game::checkIfEndOfGame()
{
    for (Column* i:board)
        if(i->getSize() == 7)
            return true;
    if (deck->size() == 0)
        return true;
    return false;
}

void Game::awardPoint()
{
    qDebug () << "awarding point to player " << whoseTurn;
    players[whoseTurn]->incGuessedCards();
}

/*! \brief set columns that can be clicked;
 * TODO uzaleznienie tego od tego, jaka karta zostala wybrana, jak na razie po prostu
 * pozwala wrzucac je wszedzie
 */
void Game::setEnabledColumns(int whoseTurn, Card *c) {
    Card *card;
    if (c == NULL) {
        card = players[whoseTurn]->getCard(selectedCard); // Card selected by player.
    }
    else {
        card = c;
    }

    std::vector <bool> availableColumns;
    for (int  i = 0; i < board.size(); i++) {
        availableColumns.push_back(false);
    }

    if (card->getCardType() == CardType::STAGES_OF_LIFE || card->getColor() == trump) {
        // All columns are available.
        for (int  i = 0; i < board.size(); i++) {
            availableColumns[i] = true;
        }
    }
    else { // Only column corresponding to card type is available.
        availableColumns[whichColumn.at(card->getCardType())] = true;
    }

    assert(selectedCard == 0 || selectedCard == 1 || selectedCard == 2);
    assert(gameWindow != NULL);
    gameWindow->setEnabledColumns(availableColumns);
}

void Game::setEnabledNotEmptyColumns() {
    qDebug()<<"setEnabledNotEmptyColumns";
    assert(gameWindow != NULL);
    assert(phase == MovePhase::CARD_POWER_PHASE);
    std::vector <bool> notEmpty;
    for ( size_t i = 0; i < board.size(); i++)
        notEmpty.push_back(board[i]->getSize() > 0);
    gameWindow->setEnabledColumns(notEmpty);
    qDebug()<<"setEnabledNotEmptyColumns END";
}

void Game::updateBestScores(QString playerName, QString playerScore){
    QFile file("best_scores.csv");
    if(!file.open(QIODevice::ReadOnly)){
        assert(file.open(QIODevice::WriteOnly));
        file.close();
        assert(file.open(QIODevice::ReadOnly));
    }
    QList<QPair<QString, int>> scores;
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if(line.size() < 2) continue;
        QPair<QString, int> tmp;
        tmp.first = line.split(',').at(0);
        tmp.second = line.split(',').at(1).toInt();
        scores.push_back(tmp);
    }
    int i;
    for(i = 0; i <= scores.size(); ++i)
        if(i == scores.size() || scores[i].second < playerScore.toInt()){
            QPair<QString, int> tmp;
            tmp.first = playerName;
            tmp.second = playerScore.toInt();
            scores.insert(i, tmp);
            break;
        }
    file.close();
    assert(file.open(QIODevice::WriteOnly));
    QTextStream stream(&file);
    for(i = 0; i < scores.size() && i < NUM_OF_BEST; ++i){
        stream << scores[i].first.toUtf8() << "," << scores[i].second << ",\n";
    }
    file.close();
}

Deck* Game::getDeck() {
    return deck;
}

void Game::backToGameWindow()
{
    gameWindow->show();
}

void Game::changePhaseToStart() {
    phase = MovePhase::START;
}

void Game::setPlayedCard(Card* card) {
    playedCard = card;
}

MovePhase Game::getPhase() {
    return phase;
}

void Game::setWhoseTurnMinusOne() {
    whoseTurn = (numberOfPlayers + whoseTurn -1 ) % numberOfPlayers;
}

void Game::setPlayersNames() {
    std::vector<QString> names;
    names.push_back(players[1 - whoseTurn]->getName());
    gameWindow->setPlayersNames(names);
}

std::vector<Player*> Game::getPlayers() {
    return players;
}

int Game::getWhoseTurn() {
    return whoseTurn;
}
