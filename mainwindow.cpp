#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gamewindow.h"
#include <QDebug>
#include <QMessageBox>
#include <qdialog.h>
#include "configurationdialog.h"
#include "historydialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->buttonStartNewGame, &QAbstractButton::clicked,
            this, &MainWindow::startNewGame);
    connect(ui->buttonQuitTheGame, &QAbstractButton::clicked,
            this, &QApplication::quit);
    connect(ui->buttonHistory, &QAbstractButton::clicked,
            this, &MainWindow::showHistory);

    connect(ui->actionNewGame, &QAction::triggered,
            this, &MainWindow::startNewGame);
    connect(ui->actionQuit, &QAction::triggered,
            this, &QApplication::quit);
    connect(ui->actionHistory, &QAction::triggered,
            this, &MainWindow::showHistory);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startNewGame()
{

    //! \todo ConfigurationDialog
    ConfigurationDialog dialog(this);
    if(dialog.exec() == QDialog::Rejected) {
        return; // do nothing if dialog rejected
    }
    this->hide();
    //Get vector of players name
    std::vector<QString> playersName;
    playersName.emplace_back(dialog.player1Name());
    playersName.emplace_back(dialog.player2Name());

    gameWindow = new GameWindow(&playersName, this);
    gameWindow->show();
    qDebug() << "Create new game";
    //Create new game
}

void MainWindow::showHistory()
{
    HistoryDialog dialog(this);
    if(dialog.exec() == QDialog::Rejected) {
        return; // do nothing if dialog rejected
    }
}

void MainWindow::BackToMenu(){
    gameWindow->hide();
    this->show();
}
