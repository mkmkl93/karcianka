#ifndef GAME_H
#define GAME_H

#include <vector>
#include <map>
#include <QObject>

#include "movephase.h"
#include "cardtype.h"
#include "colours.h"
#include "colorpower.h"
#include "seapower.h"
#include "fortunepower.h"
#include "sinpower.h"
#include "stageoflifepower.h"
#include "virtuespower.h"
#include "wonderspower.h"

#define NUM_OF_BEST 10

class Column;
class Player;
class Card;
class GameWindow;
class Deck;

class Game: public QObject {
    Q_OBJECT
private:
    static std::map<enum CardType,int> whichColumn; ///< Return index of column corresponding to given card type.
    Deck* deck;
    std::vector<Column*> board;
    std::vector<Player*> players;
    Color trump; ///< Current trump color (color of last card put in color column). Cards of this color can be put in any column.
    //! \brief type of card with which card was played
    CardType cardPlayedAs;
    //! \brief card which was played this tour
    Card* playedCard;
    //! \brief the phase of move where we are
    MovePhase phase;
    int selectedCard; ///< Number of card from players hand that was selected.
    //! \brief Whose turn it is. Range [0, playersNumber - 1]
    int whoseTurn;
    int numberOfPlayers;
    GameWindow *gameWindow;

    //! \brief Calculates score for a given card.
    int pointsForCard(Card* card);
    //! \brief Set columns that can be chosen to put specific card.
    void setEnabledColumns(int whoseTurn, Card *card);
    //! \brief Set columns that are not empty as available.
    void setEnabledNotEmptyColumns();
    void setPlayersNames();
    void updateBestScores(QString playerName, QString playerScore);

public:
    Game(std::vector<QString>* players);
    ~Game() = default;
    //! \brief Changes turn to next player.
    void nextTurn();
    bool isColumnNameTaken();
    void changeTrump(Color color);
    /*!
     * \brief Checks if the game should finish.
     * \return True if the game should end, false otherwise.
     */
    bool checkIfEndOfGame();
    void awardPoint();
    void changeLastPlayedCard(Card* card);
    void replayTurn();
    void showAvailableColumns(Card* card);
    //void playerChosen();
    void selectCard(int num);
    void putCardOnBoard(Card* card);
    void putCardOnBoard(int colum);
    void takeCardFromBoard(int columnNum);
    Deck* getDeck();
    void backToGameWindow();
    void setGameWindow(GameWindow * gameWindow);
    void changePhaseToStart();
    void setPlayedCard(Card* card);
    //! \brief Ends the game
    void endGame();
    MovePhase getPhase();
    void setWhoseTurnMinusOne();
    void putCardOnBoardWithoutPower(int column);
    std::vector<Player*> getPlayers();
    int getWhoseTurn();

signals:
    //! \brief Sends game result
    void gameOver(QString);
};

#endif // GAME_H
