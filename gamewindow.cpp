#include <QDebug>
#include <QMessageBox>
#include <QPalette>
#include <QPicture>
#include "gamewindow.h"
#include "ui_gamewindow.h"
#include "clickableframe.h"
#include "fortunewindow.h"
#include "mainwindow.h"

#include <assert.h>
#include <cmath>
#include "game.h"
#include "card.h"

GameWindow::GameWindow(std::vector<QString>* playersName, MainWindow *main, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GameWindow)
{

        ui->setupUi(this);
        qDebug() << "When? " <<  ui->card0->geometry();
        prepareBoardLabels();
        for (int i =0; i < 3; i++)
            buttonClicked [i] = false;
        game = new Game(playersName);
        assert(game != NULL);
        game->setGameWindow(this);
        mainWindow = main;

        //connect buttons with cards in players hands
        connect(ui->card0, &QAbstractButton::clicked,
                this, &GameWindow::selectCard);
        connect(ui->card1, &QAbstractButton::clicked,
                this, &GameWindow::selectCard);
        connect(ui->card2, &QAbstractButton::clicked,
                this, &GameWindow::selectCard);

        //connect(ui->nextTurn, &QAbstractButton::clicked,
        //        game, &Game::nextTurn);

        //connect(ui->player1, &QAbstractButton::clicked,
        //        game, &Game::playerChosen);

        connect(game, &Game::gameOver,
                this, &GameWindow::handleEndOfGame);

        connect(ui->sin_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(0);});
        connect(ui->virtue_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(1);});
        connect(ui->stage_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(2);});
        connect(ui->sea_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(3);});
        connect(ui->color_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(4);});
        connect(ui->fortune_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(5);});
        connect(ui->wonders_column, &ClickableFrame::clicked,
                this, [this]{this->columnClicked(6);});
}

void GameWindow::prepareBoardLabels(){
    //Chyba inaczej sie nie da, sorry za ten syf :(((
    boardLabels[0][0] = ui->sin0;
    boardLabels[0][1] = ui->sin1;
    boardLabels[0][2] = ui->sin2;
    boardLabels[0][3] = ui->sin3;
    boardLabels[0][4] = ui->sin4;
    boardLabels[0][5] = ui->sin5;
    boardLabels[0][6] = ui->sin6;

    boardLabels[1][0] = ui->virtue0;
    boardLabels[1][1] = ui->virtue1;
    boardLabels[1][2] = ui->virtue2;
    boardLabels[1][3] = ui->virtue3;
    boardLabels[1][4] = ui->virtue4;
    boardLabels[1][5] = ui->virtue5;
    boardLabels[1][6] = ui->virtue6;

    boardLabels[2][0] = ui->stage0;
    boardLabels[2][1] = ui->stage1;
    boardLabels[2][2] = ui->stage2;
    boardLabels[2][3] = ui->stage3;
    boardLabels[2][4] = ui->stage4;
    boardLabels[2][5] = ui->stage5;
    boardLabels[2][6] = ui->stage6;

    boardLabels[3][0] = ui->sea0;
    boardLabels[3][1] = ui->sea1;
    boardLabels[3][2] = ui->sea2;
    boardLabels[3][3] = ui->sea3;
    boardLabels[3][4] = ui->sea4;
    boardLabels[3][5] = ui->sea5;
    boardLabels[3][6] = ui->sea6;

    boardLabels[4][0] = ui->color0;
    boardLabels[4][1] = ui->color1;
    boardLabels[4][2] = ui->color2;
    boardLabels[4][3] = ui->color3;
    boardLabels[4][4] = ui->color4;
    boardLabels[4][5] = ui->color5;
    boardLabels[4][6] = ui->color6;

    boardLabels[5][0] = ui->fortune0;
    boardLabels[5][1] = ui->fortune1;
    boardLabels[5][2] = ui->fortune2;
    boardLabels[5][3] = ui->fortune3;
    boardLabels[5][4] = ui->fortune4;
    boardLabels[5][5] = ui->fortune5;
    boardLabels[5][6] = ui->fortune6;

    boardLabels[6][0] = ui->wonders0;
    boardLabels[6][1] = ui->wonders1;
    boardLabels[6][2] = ui->wonders2;
    boardLabels[6][3] = ui->wonders3;
    boardLabels[6][4] = ui->wonders4;
    boardLabels[6][5] = ui->wonders5;
    boardLabels[6][6] = ui->wonders6;

    columns[0] = ui->sin_column;
    columns[1] = ui->virtue_column;
    columns[2] = ui->stage_column;
    columns[3] = ui->sea_column;
    columns[4] = ui->color_column;
    columns[5] = ui->fortune_column;
    columns[6] = ui->wonders_column;
}

void GameWindow::handleEndOfGame(QString endStatement)
{
    QMessageBox::information(this, "Winner", endStatement);

}

GameWindow::~GameWindow()
{
    delete ui;
}

void GameWindow::setCards(std::vector<Card*> cards){
    assert(cards.size() == 3);
    qDebug() << cards[0]->toString();
    qDebug() << cards[1]->toString();
    qDebug() << cards[2]->toString();
    QIcon icon0(QPixmap(cards[0]->getFilePath()));
    ui->card0->setIcon(icon0);
    qDebug() << "SIZE: " << ui->card0->size();
    ui->card0->setIconSize(ui->card0->size());

    QIcon icon1(QPixmap(cards[1]->getFilePath()));
    ui->card1->setIcon(icon1);
    ui->card1->setIconSize(ui->card1->size());

    QIcon icon2(QPixmap(cards[2]->getFilePath()));
    ui->card2->setIcon(icon2);
    ui->card2->setIconSize(ui->card2->size());
}

void GameWindow::putCardOnBoard(Card* card, int column, int row){
    assert(column >= 0 && column < 7 && row >=0 && row < 7);
    QPixmap pixmap(card->getFilePath());
    pixmap = pixmap.scaled(boardLabels[column][row]->geometry().width(), boardLabels[column][row]->geometry().height());
    boardLabels[column][row]->setPixmap(pixmap);
    boardLabels[column][row]->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
}

void GameWindow::takeCardFromBoard(int column, int row){
    assert(column >= 0 && column < 7 && row >=0 && row < 7);
    QPixmap pixmap;
    boardLabels[column][row]->setPixmap(pixmap);
}
void GameWindow::setPlayersNames(std::vector<QString> names){
    ui->player->setText("Gracz: " + names[0]);
}

void GameWindow::setEnabledColumns(std::vector<bool> map){
    assert(map.size() == 7);
    QPalette pal = palette();
    pal.setColor(QPalette::Background, Qt::green);
    for(int i = 0; i < 7; ++i){
        if(map[i]){
            columns[i]->setActive(1);
            columns[i]->setStyleSheet("border: 0;background-color:lightgreen;");
        } else {
            columns[i]->setActive(0);
            columns[i]->setStyleSheet("border: 0;");
        }
    }
}

void GameWindow::columnClicked(int num){
    qDebug() << "clicked: " << num;
    if(columns[num]->getActive()){
        // only use: seapower
        switch(game->getPhase()){
            case MovePhase::CARD_POWER_PHASE:{
                game->takeCardFromBoard(num);
                setEnabledColumns({0, 0, 0, 0, 0, 0, 0});
                return;
            }
            case MovePhase::SINS_PHASE:{
                game->putCardOnBoardWithoutPower(num);
                setEnabledColumns({0, 0, 0, 0, 0, 0, 0});
                return;
            }
            case MovePhase::PUT_ON_BOARD:{
                setEnabledColumns({0, 0, 0, 0, 0, 0, 0});
                game->putCardOnBoard(num);
            }
            default:
                break;
        }
    }
}

void GameWindow::showFortunes() {
 this->hide();
 fortuneWindow = new FortuneWindow(this, game);
 fortuneWindow->show();
 qDebug() << "Create new fortune window";
 }

void GameWindow::showWonders() {
 this->hide();
 wondersWindow = new WondersWindow(this, game);
 wondersWindow->show();
 qDebug() << "Create new fortune window";
 }

void GameWindow::selectCard(){

    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender()); // retrieve the button you have clicked
    QString buttonText = (buttonSender->objectName()).right(1); // retrive the text from the button clicked
    setEnabledColumns({0, 0, 0, 0, 0, 0, 0});

    if (!buttonClicked[buttonText.toInt()]){
        if (buttonClicked[0] || buttonClicked[1] || buttonClicked[2])
            game->changePhaseToStart();
        setButtonsColor();
        buttonClicked[buttonText.toInt()] = true;
        game->selectCard(buttonText.toInt());
    }
    else{
        buttonClicked[buttonText.toInt()] = false;
        game->changePhaseToStart();
    }
}
void GameWindow::setButtonsColor(){
    for (int i =0; i <3; i++){
        buttonClicked[i] = false;
    }
}



