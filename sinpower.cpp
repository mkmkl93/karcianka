#include "sinpower.h"
#include "game.h"
#include "player.h"
#include "card.h"
#include "column.h"
#include <QDebug>

SinPower::SinPower()
{
    //qDebug() << " sins power created ";
}
void SinPower::printPower(){
    qDebug() << " sins power ";
}
void SinPower::usePower(Player* player, Game* game)
{
    qDebug() << "SIN POWER  used";
    if (game->getDeck()->size() == 0){
        game->endGame();
        return;
    }
    srand (time(NULL));
    int cardNum = rand() % 3;
    qDebug() << "SIN POWER  removed " << player->getCard(cardNum)->toString();
    game->setPlayedCard(player->getCard(cardNum));
    player->removeCard(cardNum);
    player->drawCard();
    player->printHand();
}
void SinPower::usePower(Game* game){
    qDebug()<< "wrong power: sins";
}
void SinPower::usePower(Column* column,Game* game){
    qDebug()<< "wrong power: sins";
}
