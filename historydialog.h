#ifndef HISTORYDIALOG_H
#define HISTORYDIALOG_H

#include <QDialog>
#include <QLabel>

namespace Ui {
class HistoryDialog;
}

class HistoryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HistoryDialog(QWidget *parent = nullptr);
    ~HistoryDialog();
    void loadRecords();

private:
    Ui::HistoryDialog *ui;
    QLabel * names[10];
    QLabel * scores[10];
};

#endif // HISTORYDIALOG_H
