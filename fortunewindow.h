#ifndef FORTUNEWINDOW_H
#define FORTUNEWINDOW_H

#include <QDialog>
#include "colours.h"

class Game;

namespace Ui {
class FortuneWindow;
}

class FortuneWindow : public QDialog
{
    Q_OBJECT
public:
    explicit FortuneWindow(QWidget *parent = nullptr, Game* game = nullptr);
    ~FortuneWindow();
    void showWin();
    void showLose();
    void guessColour(Color c);
    void guessNum(int num);

private:
    Ui::FortuneWindow *ui;
    Game* game;
};

#endif // FORTUNEWINDOW_H
