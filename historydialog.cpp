#include <QFile>
#include <QDebug>

#include "historydialog.h"
#include "ui_historydialog.h"

HistoryDialog::HistoryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HistoryDialog)
{
    ui->setupUi(this);
    names[0] = ui->name0;
    names[1] = ui->name1;
    names[2] = ui->name2;
    names[3] = ui->name3;
    names[4] = ui->name4;
    names[5] = ui->name5;
    names[6] = ui->name6;
    names[7] = ui->name7;
    names[8] = ui->name8;
    names[9] = ui->name9;

    scores[0] = ui->score0;
    scores[1] = ui->score1;
    scores[2] = ui->score2;
    scores[3] = ui->score3;
    scores[4] = ui->score4;
    scores[5] = ui->score5;
    scores[6] = ui->score6;
    scores[7] = ui->score7;
    scores[8] = ui->score8;
    scores[9] = ui->score9;

    loadRecords();
}

HistoryDialog::~HistoryDialog()
{
    delete ui;
}

void HistoryDialog::loadRecords(){
    qDebug() << "maybe";
    QFile file("best_scores.csv");
    if(!file.open(QIODevice::ReadOnly))
        return; //do nothing if no record
    qDebug() << "There is sth";

    int i = 0;
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if(line.size() < 2) continue;
        names[i]->setText(line.split(',').at(0));
        scores[i]->setText(line.split(',').at(1));
        ++i;
    }
}
