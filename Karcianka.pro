#-------------------------------------------------
#
# Project created by QtCreator 2019-03-27T18:29:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Karcianka
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    configurationdialog.cpp \
    gamewindow.cpp \
    game.cpp \
    player.cpp \
    field.cpp \
    deck.cpp \
    column.cpp \
    card.cpp \
    power.cpp \
    colorpower.cpp \
    clickableframe.cpp \
    sinpower.cpp \
    fortunewindow.cpp \
    fortunepower.cpp \
    seapower.cpp \
    virtuespower.cpp \
    stageoflifepower.cpp \
    wonderspower.cpp \
    wonderswindow.cpp \
    historydialog.cpp


HEADERS += \
    cardtype.h \
        mainwindow.h \
    configurationdialog.h \
    gamewindow.h \
    game.h \
    player.h \
    field.h \
    deck.h \
    column.h \
    card.h \
    power.h \
    colorpower.h \
    colours.h \
    clickableframe.h \
    sinpower.h \
    fortunewindow.h \
    fortunepower.h \
    seapower.h \
    virtuespower.h \
    stageoflifepower.h \
    wonderspower.h \
    movephase.h \
    wonderswindow.h \
    historydialog.h

FORMS += \
        mainwindow.ui \
    configurationdialog.ui \
    gamewindow.ui \
    fortunewindow.ui \
    wonderswindow.ui \
    historydialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc \
    cards.qrc
