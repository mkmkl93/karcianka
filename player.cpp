#include <QDebug>
#include "player.h"
#include "game.h"
#include <assert.h>

Player::Player(QString &name, Game * game): game(game), guessedCards(0), name(name) {
    cards = game->getDeck()->getThreeCards();
}

//void Player::playCard(int number){
//    cards[number]->chosen();
//}

void Player::drawCard(){
    Deck* deck = game->getDeck();
    if (deck->size() == 0)
        return;
    addCard(deck->getOneCard());
}

void Player::addCard(Card* card){
    int positionToAdd = -1;
    for (unsigned int i =0; i< cards.size(); i++)
        if (cards[i] == NULL)
            positionToAdd = i;
    assert (positionToAdd >= 0);
    cards[positionToAdd] = card;
}

void Player::removeCard(int number){
    assert(number >= 0 && (unsigned int)number < cards.size());
    cards[number] = NULL;
}

std::vector<Card*> Player::getCards(){
    return this->cards;
}

int Player::getGuessedCards()
{
    return guessedCards;
}

Card * Player::getCard(int number){
    assert(number >= 0 && (unsigned int) number < cards.size());
    return this->cards[number];
}

void Player::printHand(){
    qDebug() << "printHand " << cards.size() ;
    for ( int i =0; i < 3; i++ ){
        if ( cards[i] != NULL )
           qDebug() <<i <<" : "<<cards[i]->toString();
        else
            qDebug() <<i <<" : "<<"NULL";

    }
}
void Player::incGuessedCards()
{
    guessedCards++;
}
QString Player::getName(){
    return name;
}
