#ifndef MOVEPHASE_H
#define MOVEPHASE_H

//, END_TURN chyba zbędne jeżeli usuniemy przycisk next
/*!
 * START = Beginning of turn, waiting for player to choose card that will be played.
 * PUT_ON_BOARD = Waiting for player to choose column where card will be put.
 * CARD_POWER_PHASE = Card was played, waiting for button click (cloumn/player/next) depending on type of played card.
    * SINS_PHASE = Wait for player to be choosen.
    * VIRTUES_PHASE = Player has extra turn.
    * STAGES_OF_LIFE_PHASE = No advantage for player, trump color doesn't change when card is put in color column.
    * SEAS_PHASE = Waiting for player to choose column, to take card from board.
    * COLORS_PHASE = Trump color changes to color of played card.
    * FORTUNES_PHASE = popup with own buttons (numbers/colores),
    * WONDERS_PHASE = Popup showing cards from deck/other player's hand.
 * END_TURN = Waiting for buton "next" to be clicked.
*/
enum class MovePhase {START, PUT_ON_BOARD, CARD_POWER_PHASE, SINS_PHASE};
#endif // MOVEPHASE_H
