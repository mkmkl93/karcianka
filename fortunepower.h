#ifndef FORTUNEPOWER_H
#define FORTUNEPOWER_H

#include "power.h"
#include "colours.h"

class FortuneWindow;
class Game;

class FortunePower : public Power
{
private:
    FortuneWindow* fortuneWindow;
public:
    FortunePower(FortuneWindow* fw);
    void usePower(Color guessCol, int guessNum, Game& game);
};

#endif // FORTUNEPOWER_H
