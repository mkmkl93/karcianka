#ifndef FIELD_H
#define FIELD_H

class Card;

//! \brief Class representing a single field on board. There can only be up to one card
//! on each field.
class Field {
private:
    Card* card; ///< Card that is currently on this field.
    Field();
};
#endif // FIELD_H
