#ifndef VIRTUESPOWER_H
#define VIRTUESPOWER_H
#include "power.h"

class VirtuesPower: public Power
{
public:
    VirtuesPower();
    void printPower();
    void usePower(Game* game);
    void usePower(Column *column, Game* game);
    void usePower(Player* player, Game* game);
};


#endif // VIRTUESPOWER_H
