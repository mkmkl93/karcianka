#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QLabel>
#include <QString>
#include "clickableframe.h"
#include "fortunewindow.h"
#include "wonderswindow.h"
class Game;
class Card;
class MainWindow;

namespace Ui {
class GameWindow;
}

class GameWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::GameWindow *ui;
    FortuneWindow *fortuneWindow;
    WondersWindow *wondersWindow;
    QLabel * boardLabels[7][7];
    ClickableFrame * columns[7];
    bool buttonClicked [3];


    void prepareBoardLabels();
private slots:
    void handleEndOfGame(QString endStatement);
    void columnClicked(int num);
    void selectCard();
public:
    Game* game;
    MainWindow *mainWindow;
    explicit GameWindow(std::vector<QString>* playersName, MainWindow *main, QWidget *parent = nullptr);
    ~GameWindow();
    void showGame();
    void setCards(std::vector<Card*> cards);
    void putCardOnBoard(Card* card, int column, int row);
    void setEnabledColumns(std::vector<bool> map);
    void takeCardFromBoard(int column, int row);
    void showFortunes();
    void showWonders();
    void setPlayersNames(std::vector<QString> names);
    void setButtonsColor();
};

#endif // GAMEWINDOW_H
