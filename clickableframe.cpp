#include "clickableframe.h"

ClickableFrame::ClickableFrame(QWidget* parent, Qt::WindowFlags f)
    : QFrame(parent) {
    active = false;
}

ClickableFrame::~ClickableFrame() {}

void ClickableFrame::mousePressEvent(QMouseEvent* event) {
    emit clicked();
}

void ClickableFrame::setActive(bool active){
    this->active = active;
}

bool ClickableFrame::getActive(){
    return this->active;
}
